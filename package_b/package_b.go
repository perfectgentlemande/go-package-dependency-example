package package_b

import (
	"fmt"

	"bitbucket.com/perfectgentlemande/go-package-dependency-example/package_c"
)

type SomeTypeB struct{}

func NewSomeTypeB() *SomeTypeB {
	return &SomeTypeB{}
}

func (sta *SomeTypeB) PrintB() {
	fmt.Println("Hello Package B")
}

func PrintAFromB(c package_c.SomethingFromA) {
	c.PrintA()
}
