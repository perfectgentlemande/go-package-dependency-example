package package_d

import (
	"bitbucket.com/perfectgentlemande/go-package-dependency-example/package_a"
	"bitbucket.com/perfectgentlemande/go-package-dependency-example/package_b"
)

func PrintFromA() {
	a := package_a.NewSomeTypeA()
	package_b.PrintAFromB(a)
}
