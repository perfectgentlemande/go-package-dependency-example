package package_a

import (
	"fmt"

	"bitbucket.com/perfectgentlemande/go-package-dependency-example/package_b"
)

type SomeTypeA struct{}

func NewSomeTypeA() *SomeTypeA {
	return &SomeTypeA{}
}

func (sta *SomeTypeA) PrintA() {
	fmt.Println("Hello Package A")
}

func PrintBFromA() {
	b := package_b.NewSomeTypeB()
	b.PrintB()
}
