package main

import (
	"bitbucket.com/perfectgentlemande/go-package-dependency-example/package_a"
	"bitbucket.com/perfectgentlemande/go-package-dependency-example/package_b"
)

func main() {
	a := package_a.NewSomeTypeA()
	b := package_b.NewSomeTypeB()

	a.PrintA()
	b.PrintB()
	package_a.PrintBFromA()
	//package_b.PrintAFromB()
}
